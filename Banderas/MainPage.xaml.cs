﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Banderas.Resources;
using System.Xml.Linq;
using Windows.Phone.Speech.Recognition;
using Windows.Phone.Speech.Synthesis;
using System.Windows.Media.Imaging;
using Windows.Foundation;

namespace Banderas
{
    public partial class MainPage : PhoneApplicationPage
    {
        List<BanderaItem> BanderasList = new List<BanderaItem>();
        //SpeechRecognizerUI recoWithUI;
        //SpeechSynthesizer synthesizer;
        Random random = new Random(DateTime.Now.Millisecond);
        int rand = 0;
        SpeechSynthesizer _synthesizer;                             // The speech synthesizer (text-to-speech, TTS) object
        SpeechRecognizer _recognizer;                               // The speech recognition object
        IAsyncOperation<SpeechRecognitionResult> _recoOperation;    // Used to canel the current asynchronous speech recognition operation
        bool _recoEnabled = false;                                  // When this is true, we will continue to recognize 
        Dictionary<string, String> _banderasSources;
        //int aciertos = 0;
        //int fallos = 0;

        // Constructor
        
        public MainPage()
        {
            InitializeComponent();
            inicializar();
        }

        void Parser()
        {
            XDocument xmlListaEventos = XDocument.Load("Banderas.xml");

            var items = from list in xmlListaEventos.Descendants("Bandera")
                        select new BanderaItem
                        {
                            nombre = list.Element("nombre").Value,
                            imagen = list.Element("imagen").Value
                        };

            BanderasList = (items as IEnumerable<BanderaItem>).ToList();
            habla.Visibility = System.Windows.Visibility.Visible;
            rand = random.Next(0, 10);
            BitmapImage imagenSource = new BitmapImage(new Uri(BanderasList.ElementAt(rand).imagen, UriKind.Relative));
            Flag.Source = imagenSource;
            pais.Text = BanderasList.ElementAt(rand).nombre;
            //inicializaVozEspañol();
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // Initialize the SpeechRecognizerUI object.
            /*recoWithUI = new SpeechRecognizerUI();

            // Query for a recognizer that recognizes French as spoken in France.
            IEnumerable<SpeechRecognizerInformation> spanishRecognizers = from recognizerInfo in InstalledSpeechRecognizers.All
                                                                         where recognizerInfo.Language == "es-MX"
                                                                         select recognizerInfo;

            // Set the recognizer to the top entry in the query result.
            recoWithUI.Recognizer.SetRecognizer(spanishRecognizers.ElementAt(0));

            // Create a string array of French numbers.
            string[] paises = new string[BanderasList.Count];

            for (int i = 0; i < BanderasList.Count; i++)
            {
                paises[i] = BanderasList.ElementAt(i).nombre;
            }

            // Create a list grammar from the string array and add it to the grammar set.
            recoWithUI.Recognizer.Grammars.AddGrammarFromList("Banderas", paises);

            // Load the grammar set and start recognition.
            SpeechRecognitionUIResult recoResult = await recoWithUI.RecognizeWithUIAsync();*/

            if (this._recoEnabled)
            {
                // Update the UI to the initial state
                _recoEnabled = false;
                /*
                btnContinuousRecognition.Content = "Start speech recognition";
                txtResult.Text = String.Empty;
                txtInstructions.Visibility = System.Windows.Visibility.Collapsed;
                */
                // Cancel the outstanding recognition operation, if one exists
                if (_recoOperation != null && _recoOperation.Status == AsyncStatus.Started)
                {
                    _recoOperation.Cancel();
                }
                return;
            }
            else
            {
                // Set the flag to say that we are in recognition mode
                _recoEnabled = true;

                // Update the UI
                /*
                btnContinuousRecognition.Content = "Listening... tap to cancel";
                txtInstructions.Visibility = System.Windows.Visibility.Visible;
                */
            }

            // Continuously recognize speech until the user has canceled 
            while (this._recoEnabled)
            {
                try
                {
                    // Perform speech recognition.  
                    _recoOperation = _recognizer.RecognizeAsync();
                    var recoResult = await this._recoOperation;

                    // Check the confidence level of the speech recognition attempt.
                    if (recoResult.TextConfidence < SpeechRecognitionConfidence.Medium)
                    {
                        // If the confidence level of the speech recognition attempt is low, 
                        // ask the user to try again.
                        //txtResult.Text = "Not sure what you said, please try again.";
                        await _synthesizer.SpeakTextAsync("No es del país que mencionaste intenta de nuevo");
                        pais.Visibility = System.Windows.Visibility.Collapsed;
                        //failButton.Visibility = System.Windows.Visibility.Visible;
                        //fallos++;
                        //errores.Text = fallos.ToString();
                    }
                    else
                    {
                        // Output that the color of the rectangle is changing by updating
                        // the TextBox control and by using text-to-speech (TTS). 
                        //txtResult.Text = "Changing color to: " + recoResult.Text;
                        await _synthesizer.SpeakTextAsync("Correcto pulsa siguiente");
                        pais.Visibility = System.Windows.Visibility.Visible;
                        siguiente.Visibility = System.Windows.Visibility.Visible;
                        //aciertos++;
                        //puntos.Text = aciertos.ToString();
                        // Set the fill color of the rectangle to the recognized color. 
                        //rectangleResult.Background = TryGetBrush(recoResult.Text.ToLower());
                        //goodButton.Visibility = System.Windows.Visibility.Visible;

                    }
                }
                catch (System.Threading.Tasks.TaskCanceledException)
                {
                    // Ignore the cancellation exception of the recoOperation.
                    // When recoOperation.Cancel() is called to cancel the asynchronous speech recognition operation
                    // initiated by RecognizeAsync(),  a TaskCanceledException is thrown to signify early exit.
                }
                catch (Exception err)
                {
                    // Handle the speech privacy policy error.
                    const int privacyPolicyHResult = unchecked((int)0x80045509);

                    if (err.HResult == privacyPolicyHResult)
                    {
                        MessageBox.Show("Para utilizar esta aplicación, debes aceptar la política de voz. Para hacerlo, navega a Configuración -> voz en tú teléfono y revisa que esta habilitado el servicio de reconocimiento de voz");
                        _recoEnabled = false;
                        //btnContinuousRecognition.Content = "Start speech recognition";
                    }
                    else
                    {
                        //txtResult.Text = "Error: " + err.Message;
                    }
                }
            }

        }
        /*
        public async void inicializaVozEspañol()
        {
            _synthesizer = new SpeechSynthesizer();
            IEnumerable<VoiceInformation> englishVoices = from voice in InstalledVoices.All
                                                          where voice.Language == "es-MX"
                                                          where voice.Gender == VoiceGender.Female
                                                          select voice;

            // Set the voice as identified by the query.
            _synthesizer.SetVoice(englishVoices.ElementAt(0));
            await _synthesizer.SpeakTextAsync("¿De que país es esta bandera?");

        }*/

        public async void inicializar()
        {
            Parser();

            string[] paises = new string[BanderasList.Count];

            for (int i = 0; i < BanderasList.Count; i++)
            {
                paises[i] = BanderasList.ElementAt(i).nombre;
            }

            if (_banderasSources == null)
            {
                _banderasSources = new Dictionary<string, String>();

                for (int i = 0; i < BanderasList.Count; i++)
                {
                    /*
                    _colorBrushes.Add("Alemania", paises[0]);
                    _colorBrushes.Add("Argentina", paises[1]);
                    _colorBrushes.Add("Brasil", paises[2]);
                    _colorBrushes.Add("Cánada", paises[3]);
                    _colorBrushes.Add("China", paises[4]);
                    _colorBrushes.Add("Cuba", paises[5]);
                    _colorBrushes.Add("España", paises[6]);
                    _colorBrushes.Add("Holanda", paises[7]);
                    _colorBrushes.Add("Italia", paises[8]);
                    _colorBrushes.Add("México", paises[9]);
                    _colorBrushes.Add("Rusia", paises[8]);
                    */
                    _banderasSources.Add(BanderasList.ElementAt(i).nombre, paises[i]);
                }
            }

            try
            {
                // Create the speech recognizer and speech synthesizer objects. 
                if (_synthesizer == null)
                {
                    _synthesizer = new SpeechSynthesizer();
                    IEnumerable<VoiceInformation> englishVoices = from voice in InstalledVoices.All
                                                                  where voice.Language == "es-MX"
                                                                  where voice.Gender == VoiceGender.Male
                                                                  select voice;

                    // Set the voice as identified by the query.
                    _synthesizer.SetVoice(englishVoices.ElementAt(0));
                    await _synthesizer.SpeakTextAsync("¿De que país es esta bandera?, presiona el botón hablar para comenzar, pulsa de nuevo para detener el juego");
                }
                if (_recognizer == null)
                {
                    _recognizer = new SpeechRecognizer();

                    var _spanishRecognizer = InstalledSpeechRecognizers.All.FirstOrDefault(d => d.Language.ToUpper() == "ES-MX");

                    _recognizer.SetRecognizer(_spanishRecognizer);
                    // Set up a list of colors to recognize.
                    _recognizer.Grammars.AddGrammarFromList("Banderas", _banderasSources.Keys);
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
            }
            //inicializaVozEspañol();
            habla.Visibility = System.Windows.Visibility.Visible;
        }

        private void siguiente_Click(object sender, RoutedEventArgs e)
        {
            siguiente.Visibility = System.Windows.Visibility.Collapsed;
            pais.Visibility = System.Windows.Visibility.Collapsed;
            Parser();
        }
    }
}